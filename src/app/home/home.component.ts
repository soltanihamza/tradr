import { Component, OnInit } from '@angular/core';
import { ElementService } from '../element-service';
import { RestService } from '../rest-service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.sass'],
  providers:[ElementService,RestService]
})
export class HomeComponent implements OnInit {

  elements:{name:string}[]=[];

  constructor(private elementService:ElementService,private restService:RestService) { }

  ngOnInit() {
    this.elements=this.elementService.elements;
    console.log("test");
    this.restService.getUsers().subscribe(data=>{
      var content = JSON.parse(data['_body']);
      content.forEach(element => {
        console.log(element.name);
        this.elements.push(element.name);
      });
      });

  }

}
