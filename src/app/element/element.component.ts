import { Component, OnInit, Input } from '@angular/core';
import { ElementService } from '../element-service';

@Component({
  selector: 'app-element',
  templateUrl: './element.component.html',
  styleUrls: ['./element.component.sass']
})
export class ElementComponent implements OnInit {

  @Input() book:{name:string};
  constructor(private elementService:ElementService) { }

  ngOnInit() {
  }

}
