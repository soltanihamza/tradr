import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';

@Injectable()
export class RestService {
  constructor(private http:Http){}
  getUsers(){
    return this.http.get('http://localhost:8080/api/v1/items');
  }
}
